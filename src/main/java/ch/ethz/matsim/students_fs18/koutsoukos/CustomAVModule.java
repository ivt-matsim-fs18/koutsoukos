package ch.ethz.matsim.students_fs18.koutsoukos;

import java.util.Collections;

import org.matsim.api.core.v01.TransportMode;
import org.matsim.api.core.v01.network.Network;
import org.matsim.contrib.dvrp.run.DvrpModule;
import org.matsim.core.controler.AbstractModule;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.network.algorithms.TransportModeNetworkFilter;

import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

public class CustomAVModule extends AbstractModule {
	@Override
	public void install() {
	}

	@Provides
	@Singleton
	@Named(DvrpModule.DVRP_ROUTING)
	public Network provideAVNetwork(Network network) {
		TransportModeNetworkFilter filter = new TransportModeNetworkFilter(network);
		Network filtered = NetworkUtils.createNetwork();
		filter.filter(filtered, Collections.singleton(TransportMode.car));
		return filtered;
	}
}
